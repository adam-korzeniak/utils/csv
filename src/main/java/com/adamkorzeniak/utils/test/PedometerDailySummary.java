package com.adamkorzeniak.utils.test;

import com.adamkorzeniak.utils.csv.parser.annotation.CsvColumn;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class PedometerDailySummary {

    //    @CsvColumn(name = "create_time")
    @CsvColumn(position = 0)
    private LocalDateTime createTime;

    //    @CsvColumn(name = "update_time")
    @CsvColumn(position = 1)
    private LocalDateTime updateTime;

    //    @CsvColumn(name = "day_time")
    @CsvColumn(position = 2)
    private Long date;

    //    @CsvColumn(name = "step_count")
    @CsvColumn(position = 3)
    private Integer stepCount;

    //    @CsvColumn(name = "run_step_count")
    @CsvColumn(position = 4)
    private Integer runStepCount;

    //    @CsvColumn(name = "walk_step_count")
    @CsvColumn(position = 5)
    private Integer walkStepCount;

    //    @CsvColumn(name = "distance")
    @CsvColumn(position = 6)
    private Double distanced;

    //    @CsvColumn(name = "calorie")
    @CsvColumn(position = 7)
    private Double calorie;

    //    @CsvColumn(name = "active_time")
    @CsvColumn(position = 8)
    private Long activeTime;

}
