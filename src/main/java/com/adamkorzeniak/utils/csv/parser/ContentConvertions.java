package com.adamkorzeniak.utils.csv.parser;

import com.adamkorzeniak.utils.csv.parser.exception.DefaultInternalException;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;

@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
class ContentConvertions {
    private final CsvReaderConfig csvReaderConfig;

    private final Map<Class<?>, BiFunction<String, ConvertionParams, Object>> MAPPING_FUNCTIONS = Map.of(
            String.class, ContentConvertions::toString,
            BigDecimal.class, ContentConvertions::toBigDecimal,
            Double.class, ContentConvertions::toDouble,
            double.class, ContentConvertions::toDouble,
            Long.class, ContentConvertions::toLong,
            long.class, ContentConvertions::toLong,
            Integer.class, ContentConvertions::toInteger,
            int.class, ContentConvertions::toInteger,
            LocalDateTime.class, this::toDate
    );

    BiFunction<String, ConvertionParams, Object> getMappingFunction(Class<?> clazz) {
        return Optional.ofNullable(MAPPING_FUNCTIONS.get(clazz))
                .orElseThrow(() -> new DefaultInternalException(String.format("Missing mapping for fieldType: %s", clazz)));
    }

    boolean isSupportedType(Class<?> clazz) {
        return MAPPING_FUNCTIONS.containsKey(clazz);
    }

    private static String toString(String value, ConvertionParams params) {
        return value;
    }

    private static Double toDouble(String value, ConvertionParams params) {
        return Optional.ofNullable(value)
                .map(Double::parseDouble)
                .orElseThrow();
    }

    private static BigDecimal toBigDecimal(String value, ConvertionParams params) {
        return Optional.ofNullable(value)
                .map(BigDecimal::new)
                .orElseThrow();
    }

    private static Integer toInteger(String value, ConvertionParams params) {
        return Optional.ofNullable(value)
                .map(Integer::parseInt)
                .orElseThrow();
    }

    private static Long toLong(String value, ConvertionParams params) {
        return Optional.ofNullable(value)
                .map(Long::parseLong)
                .orElseThrow();
    }

    private LocalDateTime toDate(String value, ConvertionParams params) {
        String dateTimeFormat = params.getDateFormat()
                .orElse(csvReaderConfig.dateTimeFormat());
        return Optional.ofNullable(value)
                .map(v -> LocalDateTime.parse(v, DateTimeFormatter.ofPattern(dateTimeFormat)))
                .orElseThrow();
    }
}
