package com.adamkorzeniak.utils.csv.parser;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;
import lombok.RequiredArgsConstructor;

import java.io.IOException;

@RequiredArgsConstructor
class CsvPreprocessor {
    private final CsvReaderConfig config;

    void skipLines(CSVReader csvReader) throws IOException, CsvValidationException {
        for (int i = 0; i < config.skipLines(); i++) {
            csvReader.readNext();
        }
    }
}
