package com.adamkorzeniak.utils.csv.parser;

import java.util.List;

record ModelRepresentation<T>(
        Class<T> clazz,
        List<FieldDetails> fieldDetails
) {}
