package com.adamkorzeniak.utils.csv.parser;

import com.adamkorzeniak.utils.csv.parser.annotation.CsvColumn;
import lombok.experimental.UtilityClass;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.stream.Stream;

@UtilityClass
class ReflectionUtils {

    Stream<Field> getCsvColumnFields(Class<?> clazz) {
        return Arrays.stream(clazz.getDeclaredFields())
                .filter(field -> !Modifier.isStatic(field.getModifiers()))
                .filter(field -> field.getAnnotation(CsvColumn.class) != null);
    }
}
