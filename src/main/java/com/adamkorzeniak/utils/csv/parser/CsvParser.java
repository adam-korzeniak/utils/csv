package com.adamkorzeniak.utils.csv.parser;

import com.adamkorzeniak.utils.csv.parser.exception.CsvProcessingException;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class CsvParser {

    private final ModelValidator modelValidator;
    private final CsvPreprocessor preprocessor;
    private final CsvProcessor processor;

    CsvParser(CsvReaderConfig config) {
        ContentConvertions contentConvertions = new ContentConvertions(config);
        this.modelValidator = new ModelValidator(contentConvertions, config);
        this.preprocessor = new CsvPreprocessor(config);
        this.processor = new CsvProcessor(contentConvertions, config);
    }

    public static CsvReaderConfigBuilder builder() {
        return new CsvReaderConfigBuilder();
    }

    public <T> List<T> read(File file, Class<T> clazz) throws CsvProcessingException {
        modelValidator.validate(clazz);
        try (CSVReader csvReader = new CSVReader(new FileReader(file))) {
            CsvInput<T> input = new CsvInput<>(csvReader, clazz);
            preprocessor.skipLines(input.getReader());
            return processor.read(input);
        } catch (FileNotFoundException e) {
            throw new CsvProcessingException("File could not be found", e);
        } catch (IOException e) {
            throw new CsvProcessingException("Could not read file", e);
        } catch (CsvValidationException e) {
            throw new CsvProcessingException("File csv structure is invalid", e);
        }
    }

}
