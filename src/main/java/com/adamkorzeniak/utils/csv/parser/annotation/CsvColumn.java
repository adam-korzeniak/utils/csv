package com.adamkorzeniak.utils.csv.parser.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({FIELD})
@Retention(RUNTIME)
public @interface CsvColumn {

    //TODO: Check if empty name and position or both filled name and position throw exception
    String name() default "";

    int position() default -1;

    //TODO: Currently no support, all fields are required
    boolean nullable() default false;

    //TODO: Check dateTimeFormatter is ignored if null
    String dateTimeFormatter() default "";
}
