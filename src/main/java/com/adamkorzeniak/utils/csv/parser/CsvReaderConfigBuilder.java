package com.adamkorzeniak.utils.csv.parser;

public class CsvReaderConfigBuilder {
    private static final String DEFAULT_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS";

    private int skipLines;
    private boolean headersIncluded;
    private String dateTimeFormat;

    CsvReaderConfigBuilder() {
        this.dateTimeFormat = DEFAULT_DATE_TIME_FORMAT;
        this.skipLines = 0;
        this.headersIncluded = true;
    }

    public CsvReaderConfigBuilder skipLines(int skipLines) {
        this.skipLines = skipLines;
        return this;
    }

    public CsvReaderConfigBuilder headersIncluded(boolean headersIncluded) {
        this.headersIncluded = headersIncluded;
        return this;
    }

    public CsvReaderConfigBuilder dateTimeFormat(String dateTimeFormat) {
        this.dateTimeFormat = dateTimeFormat;
        return this;
    }

    public CsvParser build() {
        var config = new CsvReaderConfig(skipLines, headersIncluded, dateTimeFormat);
        return new CsvParser(config);
    }

}
