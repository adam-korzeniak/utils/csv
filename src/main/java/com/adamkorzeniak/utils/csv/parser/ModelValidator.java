package com.adamkorzeniak.utils.csv.parser;

import com.adamkorzeniak.utils.csv.parser.annotation.CsvColumn;
import com.adamkorzeniak.utils.csv.parser.exception.NotSupportedFieldTypeException;
import com.adamkorzeniak.utils.csv.parser.exception.CsvProcessingException;
import lombok.RequiredArgsConstructor;

import java.lang.reflect.Field;
import java.util.Set;
import java.util.stream.Collectors;

@RequiredArgsConstructor
class ModelValidator {
    private final ContentConvertions contentConvertions;
    private final CsvReaderConfig config;

    void validate(Class<?> clazz) throws CsvProcessingException {
        validateHeaders(clazz);
        validateSupportedFieldTypes(clazz);
    }

    private void validateHeaders(Class<?> clazz) throws CsvProcessingException {
        if (!config.headersIncluded() && usesColumnNameMatching(clazz)) {
            throw new CsvProcessingException("");
        }
    }

    private boolean usesColumnNameMatching(Class<?> clazz) {
        return ReflectionUtils.getCsvColumnFields(clazz)
                .map(field -> field.getAnnotation(CsvColumn.class))
                .anyMatch(annotation -> !annotation.name().isBlank());
    }

    private void validateSupportedFieldTypes(Class<?> clazz) throws NotSupportedFieldTypeException {
        Set<Class<?>> unsupportedFieldTypes = getUnsupportedFieldTypes(clazz);
        if (!unsupportedFieldTypes.isEmpty()) {
            throw new NotSupportedFieldTypeException(clazz, unsupportedFieldTypes);
        }
    }

    private Set<Class<?>> getUnsupportedFieldTypes(Class<?> clazz) {
        return ReflectionUtils.getCsvColumnFields(clazz)
                .map(Field::getType)
                .filter(this::isUnsupportedFieldType)
                .collect(Collectors.toSet());
    }

    private boolean isUnsupportedFieldType(Class<?> fieldType) {
        return !contentConvertions.isSupportedType(fieldType);
    }

}
