package com.adamkorzeniak.utils.csv.parser;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;

import java.util.Optional;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
class ConvertionParams {
    private final String dateFormat;

    static ConvertionParams empty() {
        return new ConvertionParams(null);
    }

    static ConvertionParams ofDateFormat(String dateFormat) {
        return new ConvertionParams(dateFormat);
    }

    Optional<String> getDateFormat() {
        return Optional.ofNullable(dateFormat);
    }

}
