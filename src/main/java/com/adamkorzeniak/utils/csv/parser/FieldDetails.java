package com.adamkorzeniak.utils.csv.parser;

record FieldDetails(
        String name,
        Class<?> clazz,
        int columnPosition
) {}
