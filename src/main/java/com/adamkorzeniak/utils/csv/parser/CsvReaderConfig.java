package com.adamkorzeniak.utils.csv.parser;

record CsvReaderConfig(
        int skipLines,
        boolean headersIncluded,
        String dateTimeFormat
) {}
