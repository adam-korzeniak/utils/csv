package com.adamkorzeniak.utils.csv.parser.exception;

public class DefaultInternalException extends RuntimeException {

    public DefaultInternalException(String message) {
        super(message);
    }

}
